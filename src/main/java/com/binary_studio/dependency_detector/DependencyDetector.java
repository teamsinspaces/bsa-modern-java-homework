package com.binary_studio.dependency_detector;

import java.util.*;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	public static boolean canBuild(DependencyList libraries) {

		final List<String> libs = libraries.libraries;
		final List<String[]> dependencies = libraries.dependencies;

		if (dependencies == null || dependencies.isEmpty()) {
			return true;
		}

		Map<String, Integer> degree = new HashMap<>();
		Map<String, List<String>> graph = new HashMap<>();
		for (String lib : libs) {
			graph.put(lib, new ArrayList<>());
			degree.put(lib, 0);
		}
		for (String[] dependency : dependencies) {
			graph.get(dependency[0]).add(dependency[1]);

			int inc = degree.get(dependency[1]) + 1;
			degree.put(dependency[1], inc);

		}

		Queue<String> queue = new LinkedList<>();
		int count = 0;

		for (String dependency : degree.keySet()) {
			if (degree.get(dependency) == 0) {
				queue.add(dependency);
				count++;
			}
		}

		while (queue.size() != 0) {
			String dependency = queue.poll();
			System.out.println(dependency);

			for (int i = 0; i < graph.get(dependency).size(); i++) {
				String pointer = (String) graph.get(dependency).get(i);

				Integer dec = degree.get(pointer) - 1;
				degree.put(pointer, dec);

				if (degree.get(pointer) == 0) {
					queue.add(pointer);
					count++;
				}
			}
		}

		return count == libs.size();
	}

	public static void main(String[] args) {

		List<String> listOfLibraries = new ArrayList<String>();
		listOfLibraries.add("foo.bar");
		listOfLibraries.add("foo.baz");
		// listOfLibraries.add("foo.quax");

		List<String[]> listOfDependency = new ArrayList<>();

		String[] str1 = new String[] { "foo.baz", "foo.bar" };
		listOfDependency.add(str1);

		// str1 = new String [] {"foo.bar", "foo.quax"};
		// listOfDependency.add(str1);

		// str1 = new String [] {"foo.quax", "foo.baz"};
		// listOfDependency.add(str1);

		DependencyList listDependency = new DependencyList(listOfLibraries, (List<String[]>) listOfDependency);

		System.out.println(canBuild(listDependency));

	}

}
