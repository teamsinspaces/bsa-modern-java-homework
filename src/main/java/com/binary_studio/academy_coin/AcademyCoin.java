package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {

	}

	public static int maxProfit(Stream<Integer> prices) {

		int[] intArray = prices.mapToInt(x -> x).toArray();

		return calculate(intArray, 0);

	}

	public static int calculate(int[] prices, int s) {
		if (s >= prices.length) {
			return 0;
		}
		int max = 0;
		for (int start = s; start < prices.length; start++) {
			int maxprofit = 0;
			for (int i = start + 1; i < prices.length; i++) {
				if (prices[start] < prices[i]) {
					int profit = calculate(prices, i + 1) + prices[i] - prices[start];
					if (profit > maxprofit) {
						maxprofit = profit;
					}
				}
			}
			if (maxprofit > max) {
				max = maxprofit;
			}
		}
		return max;
	}

}
