package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger shieldHPMax;

	private PositiveInteger curShieldHP;

	private PositiveInteger hullHPMax;

	private PositiveInteger curHullHP;

	private PositiveInteger powerGridOutput;

	private PositiveInteger capacitorAmountMax;

	private PositiveInteger curCapacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static CombatReadyShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		return new CombatReadyShip(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate,
				speed, size, attackSubsystem, defenciveSubsystem);
	}

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHPMax = shieldHP;
		this.curShieldHP = shieldHP;
		this.hullHPMax = hullHP;
		this.curHullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmountMax = capacitorAmount;
		this.curCapacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override
	public void endTurn() {
		var recharCapaAmo = this.capacitorRechargeRate.value() + this.curCapacitorAmount.value();
		var checkCapaAmo = Math.min(recharCapaAmo, this.capacitorAmountMax.value());
		this.curCapacitorAmount = PositiveInteger.of(checkCapaAmo);
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		var attacReqCharAmo = this.attackSubsystem.getCapacitorConsumption().value();

		if (attacReqCharAmo <= this.curCapacitorAmount.value()) {
			this.curCapacitorAmount = PositiveInteger.of(this.curCapacitorAmount.value() - attacReqCharAmo);
			var damage = this.attackSubsystem.attack(target);
			var attackAc = new AttackAction(damage, this, target, this.attackSubsystem);
			return Optional.of(attackAc);
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		var reducAttack = this.defenciveSubsystem.reduceDamage(attack);
		var damage = reducAttack.damage;

		var shielHpRema = this.curShieldHP.value() - damage.value();
		if (shielHpRema >= 0) {
			this.curShieldHP = PositiveInteger.of(shielHpRema);
			return new AttackResult.DamageRecived(reducAttack.weapon, damage, this);
		}

		var remainDamageAmount = PositiveInteger.of(Math.abs(shielHpRema));
		this.curShieldHP = PositiveInteger.of(0);

		var hullHPRemain = this.curHullHP.value() - remainDamageAmount.value();
		if (hullHPRemain > 0) {
			this.curHullHP = PositiveInteger.of(hullHPRemain);
			return new AttackResult.DamageRecived(reducAttack.weapon, damage, this);
		}
		return new AttackResult.Destroyed();
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		var defenseReqCharAmo = this.defenciveSubsystem.getCapacitorConsumption().value();

		System.out.print(this.curCapacitorAmount);

		if (defenseReqCharAmo <= this.curCapacitorAmount.value()) {
			this.curCapacitorAmount = PositiveInteger.of(this.curCapacitorAmount.value() - defenseReqCharAmo);
			var regerAct = this.defenciveSubsystem.regenerate();
			var hullRegeneAct = regenerHull(regerAct);
			var shieldRegeneAct = regenerShiel(hullRegeneAct);
			return Optional.of(shieldRegeneAct);
		}
		else {
			return Optional.empty();
		}
	}

	private RegenerateAction createRegenerAction(PositiveInteger shieldRegenerated, PositiveInteger hullRegenerated) {
		return new RegenerateAction(shieldRegenerated, hullRegenerated);
	}

	private RegenerateAction regenerHull(RegenerateAction regenerateAction) {
		var missinHullHp = this.hullHPMax.value() - this.curHullHP.value();
		var availableHullRegenHp = regenerateAction.hullHPRegenerated;

		int regeneratedHullHP = 0;

		if (missinHullHp != 0) {
			regeneratedHullHP = (missinHullHp > availableHullRegenHp.value()) ? availableHullRegenHp.value()
					: missinHullHp;
			this.curHullHP = PositiveInteger.of(regeneratedHullHP);
		}

		return createRegenerAction(regenerateAction.shieldHPRegenerated, PositiveInteger.of(regeneratedHullHP));
	}

	private RegenerateAction regenerShiel(RegenerateAction regenerateAction) {
		var missinShielHp = this.shieldHPMax.value() - this.curShieldHP.value();
		var availableShielRegenHp = regenerateAction.shieldHPRegenerated;

		int regeneratedShielHP = 0;

		if (missinShielHp != 0) {
			regeneratedShielHP = (missinShielHp > availableShielRegenHp.value()) ? availableShielRegenHp.value()
					: missinShielHp;
			this.curHullHP = PositiveInteger.of(regeneratedShielHP);
		}

		return createRegenerAction(PositiveInteger.of(regeneratedShielHP), regenerateAction.hullHPRegenerated);
	}

}
