package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger powerGridOutput;

	private PositiveInteger capacitorAmount;

	private PositiveInteger capacitorRechargeRate;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		return new DockedShip(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powerGridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		var sysTotCon = getTotConsump(subsystem, this.defenciveSubsystem);
		var bothsysFitShipOut = checkSysTotPgCon(sysTotCon);

		if (bothsysFitShipOut) {
			this.attackSubsystem = subsystem;
		}
		else {
			var missPoer = sysTotCon.value() - this.powerGridOutput.value();
			throw new InsufficientPowergridException(missPoer);
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		var sysTotCon = getTotConsump(this.attackSubsystem, subsystem);
		var bothsysFitShipOut = checkSysTotPgCon(sysTotCon);

		if (bothsysFitShipOut) {
			this.defenciveSubsystem = subsystem;
		}
		else {
			var missPoer = sysTotCon.value() - this.powerGridOutput.value();
			throw new InsufficientPowergridException(missPoer);
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {

		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return CombatReadyShip.construct(this.name, this.shieldHP, this.hullHP, this.powerGridOutput,
				this.capacitorAmount, this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem,
				this.defenciveSubsystem);
	}

	private PositiveInteger getTotConsump(AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		var attaCSysPgCons = (attackSubsystem == null) ? 0 : attackSubsystem.getPowerGridConsumption().value();
		var defenSysPgCons = (defenciveSubsystem == null) ? 0 : defenciveSubsystem.getPowerGridConsumption().value();

		return PositiveInteger.of(attaCSysPgCons + defenSysPgCons);
	}

	private boolean checkSysTotPgCon(PositiveInteger sysTotPgCon) {
		return sysTotPgCon.value() <= this.powerGridOutput.value();
	}

}
